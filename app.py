"""Main module"""
from flask import Flask, render_template, request, jsonify

from extensions import db, migrate
from processor import Processor

app = Flask(__name__)
app.config[
    'SQLALCHEMY_DATABASE_URI'
] = 'postgresql://dev:devpass@student-registration_db/student_registration'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
migrate.init_app(app, db)


@app.route('/faculty/add')
def add_faculty():
    """Function for adding faculties"""
    faculty_id = Processor.add_faculty(request.json)
    return jsonify({'ok': True, 'id': faculty_id})


@app.route('/group/add')
def add_group():
    group_id = Processor.add_group(request.json)
    return jsonify({'ok': True, 'id': group_id})


@app.route('/student/add')
def add_student():
    student_id = Processor.add_student(request.json)
    return jsonify({'ok': True, 'id': student_id})


@app.route('/student/expel/<student_id>')
def expel_student(student_id):
    Processor.expel_student(student_id)
    return jsonify({'ok': True})


@app.route('/')
def student_list():
    students = Processor.list_students()
    return render_template('student_list.html', students=[{
        'email': student.email,
        'first_name': student.first_name,
        'last_name': student.last_name
    } for student in students])


if __name__ == '__main__':
    app.run()
