import unittest

from app import app
from extensions import db
from models import Faculty, Group, Student
from processor import Processor
from sqlalchemy.exc import SQLAlchemyError


class BasicTest(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
        context = app.app_context()
        context.push()
        db.init_app(app)
        db.create_all()

    def tearDown(self):
        db.drop_all()

    def test_smoke(self):
        faculty_id = Processor.add_faculty({'name': 'Факультет тестирования ПО'})
        group_id = Processor.add_group({'name': 'D412F', 'faculty_id': faculty_id})
        student_id = Processor.add_student({
            'email': 'tester@test.com',
            'first_name': '123',
            'last_name': '456',
            'group_id': group_id
        })

        faculty = Faculty.query.get(faculty_id)
        group = Group.query.get(group_id)
        student = Student.query.get(student_id)

        self.assertEqual(faculty.name, 'Факультет тестирования ПО')
        self.assertEqual(group.name, 'D412F')
        self.assertEqual(student.last_name, '456')

    def test_expulsion(self):
        faculty_id = Processor.add_faculty({'name': 'Факультет тестирования ПО'})
        group_id = Processor.add_group({'name': 'D412F', 'faculty_id': faculty_id})
        student_id = Processor.add_student({
            'email': 'testor@test.com',
            'first_name': '123',
            'last_name': '456',
            'group_id': group_id
        })

        Processor.expel_student(student_id)

        student = Student.query.get(student_id)
        self.assertTrue(student.expelled)

    def test_faculty_student(self):
        faculty_id = Processor.add_faculty({'name': 'Факультет тестирования ПО'})
        group_id = Processor.add_group({'name': 'D412F', 'faculty_id': faculty_id})
        Processor.add_student({
            'email': 'doppelganger@test.com',
            'first_name': 'Ricardo',
            'last_name': '456',
            'group_id': group_id
        })

        faculty = Faculty.query.get(faculty_id)
        student_found = [student for student in faculty.get_all_present_students() if student.first_name == 'Ricardo']
        self.assertGreater(len(student_found), 0)
