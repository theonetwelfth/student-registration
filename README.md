# Student registration  

## Overview  
This is a simple flask app for managing students (adding/expelling them,
managing groups and faculties).  
## Dependencies  
```
flask
flask-sqlalchemy
pytest
psycopg2-binary
```  

## Installation  
Requires `docker` and `docker-compose`.  
Run `docker-compose up`, wait for containers to build and voila!