from extensions import db
from models import Student, Group, Faculty


class Processor:
    @staticmethod
    def add_faculty(raw_data):
        faculty = Faculty(name=raw_data["name"])
        db.session.add(faculty)
        db.session.commit()
        return faculty.id

    @staticmethod
    def add_group(raw_data):
        group = Group(
            name=raw_data["name"], faculty=Faculty.query.get(raw_data["faculty_id"])
        )
        db.session.add(group)
        db.session.commit()
        return group.id

    @staticmethod
    def add_student(raw_data):
        student = Student(
            email=raw_data["email"],
            first_name=raw_data["first_name"],
            last_name=raw_data["last_name"],
            group=Group.query.get(raw_data["group_id"]),
        )

        db.session.add(student)
        db.session.commit()
        return student.id

    @staticmethod
    def expel_student(student_id):
        student = Student.query.get(student_id)
        student.expelled = True
        db.session.commit()

    @staticmethod
    def list_students():
        students = Student.query.all()
        return students
