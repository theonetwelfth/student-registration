FROM python:3.7-slim-stretch

WORKDIR /srv/student_registration
COPY requirements.txt /srv/student_registration/

RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["./run.sh"]