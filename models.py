from extensions import db


class Student(db.Model):
    __tablename__ = 'student'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128), unique=True, nullable=False)
    first_name = db.Column(db.String(32), nullable=False)
    last_name = db.Column(db.String(32), nullable=False)
    expelled = db.Column(db.Boolean, nullable=False, default=False)

    group_id = db.Column(db.Integer, db.ForeignKey('group.id'), nullable=False)
    group = db.relationship('Group', foreign_keys=group_id, backref=db.backref('students', lazy=True))

    def get_faculty_name(self):
        return self.group.faculty.name


class Group(db.Model):
    __tablename__ = 'group'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(8), nullable=False)

    faculty_id = db.Column(db.Integer, db.ForeignKey('faculty.id'), nullable=False)
    faculty = db.relationship('Faculty', foreign_keys=faculty_id, backref=db.backref('groups', lazy=True))

    def get_expelled_student_history(self):
        return [student for student in self.students if student.expelled]


class Faculty(db.Model):
    __tablename__ = 'faculty'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)

    def get_all_present_students(self):
        all_students = [group.students for group in self.groups]
        all_students = [student for group_list in all_students for student in group_list]
        return [student for student in all_students if not student.expelled]
